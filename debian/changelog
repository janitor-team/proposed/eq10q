eq10q (2.2~repack0-4) unstable; urgency=medium

  * Team upload
  * debian/control:
    - Bump debhelper compat to 13
    - Bump Standards-Version
  * debian/patches: Fix lv2 types (Closes: #960251)

 -- Sebastian Ramacher <sramacher@debian.org>  Sat, 16 May 2020 15:44:53 +0200

eq10q (2.2~repack0-3) unstable; urgency=medium

  [ Ondřej Nový ]
  * Team upload.
  * d/copyright: Use https protocol in Format field
  * d/control: Set Vcs-* to salsa.debian.org
  * d/changelog: Remove trailing whitespaces

  [ Felipe Sateler ]
  * Change maintainer address to debian-multimedia@lists.debian.org

  [ Ondřej Nový ]
  * d/watch: Use https protocol
  * Use debhelper-compat instead of debian/compat

  [ Sebastian Ramacher ]
  * debian/control:
    - Bump Standards-Version
    - Set RRR: no
    - Bump debhelper compat to 12

 -- Sebastian Ramacher <sramacher@debian.org>  Sat, 23 Nov 2019 15:51:26 +0100

eq10q (2.2~repack0-2.1) unstable; urgency=medium

  * Non-maintainer upload.
  * Add patch from Aurelien Jarno to fix FTBFS with glibc 2.27.
    (Closes: #890634)

 -- Adrian Bunk <bunk@debian.org>  Tue, 20 Mar 2018 19:40:58 +0200

eq10q (2.2~repack0-2) unstable; urgency=medium

  * Remove redundant amd64 cflags.

 -- Jaromír Mikeš <mira.mikes@seznam.cz>  Tue, 13 Dec 2016 15:26:26 +0100

eq10q (2.2~repack0-1) unstable; urgency=medium

  * Use copyright file for repacking.
  * New upstream version 2.2~repack0
  * Sign tags.
  * Set dh/compat 10.
  * Update copyright file.
  * Update package description.
  * Add spelling patch.
  * Forward some patches.
  * Enable sse on amd64.
  * Add patch to pass debug flags.

 -- Jaromír Mikeš <mira.mikes@seznam.cz>  Tue, 13 Dec 2016 09:12:10 +0100

eq10q (2.1~repack0-2) unstable; urgency=medium

  * Update description. (Closes: #836380)

 -- Jaromír Mikeš <mira.mikes@seznam.cz>  Sun, 04 Sep 2016 08:49:02 +0200

eq10q (2.1~repack0-1) unstable; urgency=medium

  * Imported Upstream version 2.1~repack0
  * Bump Standards.
  * Fix VCS fields.
  * Fix hardening.
  * Add patch to append debian flags correctly.

 -- Jaromír Mikeš <mira.mikes@seznam.cz>  Thu, 25 Aug 2016 14:08:14 +0200

eq10q (2.0~repack0-1) unstable; urgency=medium

  * Imported Upstream version 2.0~repack0 (Closes: #805661)
  * Repack with xz compression.
  * Refresh patches.
  * Update description.

 -- Jaromír Mikeš <mira.mikes@seznam.cz>  Sat, 19 Dec 2015 05:59:43 +0100

eq10q (2.0~beta7.1~repack0-1) unstable; urgency=medium

   [ Jaromír Mikeš ]
  * New upstream release.
  * Bump Standards.
  * Update copyright file.
  * Add lv2-dev and libfftw3-dev as build-dep.
  * Patches refreshed.
  * Add get-orig-source for repacking.

   [ Alessio Treglia ]
  * Don't sign tags.

 -- Jaromír Mikeš <mira.mikes@seznam.cz>  Sun, 22 Mar 2015 19:01:39 +0100

eq10q (2.0~beta5.1~repack0-2) unstable; urgency=low

  * Added patch to remove sse optimization.
  * Update description.

 -- Jaromír Mikeš <mira.mikes@seznam.cz>  Thu, 20 Feb 2014 22:27:55 +0100

eq10q (2.0~beta5.1~repack0-1) unstable; urgency=low

  * New upstream release.
  * Set dh/compat 9.
  * Bump Standards.
  * Added cmake as build-dep.
  * Patches removed - not applied anymore.
  * Added patch to fix prefix.
  * Removed plotmm as build-dep.
  * Update VCS urls.
  * Update copyright.
  * Update watch file.
  * Added README.source file.
  * Removed lv2-c++-tools and libpstreams-dev as build-dep.

 -- Jaromír Mikeš <mira.mikes@seznam.cz>  Mon, 17 Feb 2014 15:19:16 +0100

eq10q (1.2-2) unstable; urgency=low

  * debian/patches/01-makefile.patch:
    - Current sid's lv2-c++-tools provides static libraries needed to
      build the plugin.
  * Build-depend on lv2-c++-tools >= 1.0.4-3~
  * Remove build-dependency on libpaq-dev.

 -- Alessio Treglia <alessio@debian.org>  Sat, 26 Mar 2011 10:48:11 +0100

eq10q (1.2-1) unstable; urgency=low

  * New upstream release.
  * Added patch fixing missing prefix definition.
  * Added patches fixing instalation path to meet debian practice.
  * Delete 4-parameq.ttl.patch, not needed anymore.

 -- Jaromír Mikeš <mira.mikes@seznam.cz>  Wed, 09 Mar 2011 21:37:11 +0100

eq10q (1.1-1) unstable; urgency=low

  * Initial release (Closes: #584622).

 -- Jaromír Mikeš <mira.mikes@seznam.cz>  Fri, 14 Jan 2011 01:27:59 +0100
